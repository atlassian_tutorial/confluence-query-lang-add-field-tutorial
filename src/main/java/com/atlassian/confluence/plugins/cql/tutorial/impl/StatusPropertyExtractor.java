package com.atlassian.confluence.plugins.cql.tutorial.impl;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.cql.tutorial.PageStatus;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Collections;

import static java.util.Objects.requireNonNull;

public class StatusPropertyExtractor implements Extractor2 {
    static final String CONTENT_PROPERTY_NAME = "status";

    private final ContentPropertyManager contentPropertyManager;

    @Autowired
    public StatusPropertyExtractor(@ComponentImport ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = requireNonNull(contentPropertyManager);
    }

    @Override
    public StringBuilder extractText(Object searchable) {
        return new StringBuilder();
    }

    public Collection<FieldDescriptor> extractFields(Object searchable) {
        if (searchable instanceof Page) {
            Page page = (Page) searchable;
            String value = contentPropertyManager.getStringProperty(page, CONTENT_PROPERTY_NAME);

            if (value == null || value.isEmpty()) {
                value = PageStatus.PENDING.name();
            }

            return Collections.singletonList(StatusFields.STATUS.createField(value));
        }
        return Collections.emptyList();
    }
}
