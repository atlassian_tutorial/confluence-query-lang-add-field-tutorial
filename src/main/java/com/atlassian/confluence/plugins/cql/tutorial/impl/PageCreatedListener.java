package com.atlassian.confluence.plugins.cql.tutorial.impl;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.plugins.cql.tutorial.PageStatus;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static java.util.Objects.requireNonNull;

@Component
public class PageCreatedListener {
    private final ContentPropertyManager contentPropertyManager;
    private final EventPublisher eventPublisher;

    @Autowired
    public PageCreatedListener(@ComponentImport ContentPropertyManager contentPropertyManager,
                               @ConfluenceImport EventPublisher eventPublisher) {
        this.contentPropertyManager = requireNonNull(contentPropertyManager, "contentPropertyManager");
        this.eventPublisher = requireNonNull(eventPublisher, "eventPublisher");
    }

    @PostConstruct
    public void register() {
        eventPublisher.register(this);
    }

    @EventListener
    public void pageCreated(PageCreateEvent pageCreateEvent) {
        ContentEntityObject page = pageCreateEvent.getContent();
        contentPropertyManager.setStringProperty(page, StatusFields.STATUS.getName(), PageStatus.PENDING.name());
    }

    @PreDestroy
    public void unregister() {
        eventPublisher.unregister(this);
    }
}
