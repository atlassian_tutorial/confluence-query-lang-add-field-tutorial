package com.atlassian.confluence.plugins.cql.tutorial;

public enum PageStatus {
    PENDING("Pending"),
    REVIEW("Review"),
    COMPLETE("Complete");

    private final String humanName;

    public String humanName() {
        return humanName;
    }

    PageStatus(String humanName) {
        this.humanName = humanName;
    }
}