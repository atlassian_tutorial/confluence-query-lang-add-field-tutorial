package com.atlassian.confluence.plugins.cql.tutorial.impl;

import com.atlassian.confluence.plugins.index.api.mapping.FieldMapping;
import com.atlassian.confluence.plugins.index.api.mapping.FieldMappingsProvider;
import com.atlassian.confluence.plugins.index.api.mapping.StringFieldMapping;
import com.sun.tools.javac.util.List;

import java.util.Collection;

public class StatusFields implements FieldMappingsProvider {

    public static final StringFieldMapping STATUS = StringFieldMapping.builder("status").store(true).build();

    @Override
    public Collection<FieldMapping> getFieldMappings() {
        return List.of(STATUS);
    }
}
