package com.atlassian.confluence.plugins.cql.tutorial.rest;

import com.atlassian.confluence.plugins.cql.tutorial.PageStatus;
import com.google.common.collect.ImmutableMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Path("/")
@Consumes(value = MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class StatusValueResource {
    @Path("/status-values")
    @GET
    public Map<String, List<Map<String, String>>> getStatusValues() {
        Map<String, List<Map<String, String>>> response = new HashMap<>();
        response.put("suggestedResults", Collections.emptyList());
        response.put("searchResults", getSearchResults());
        return response;
    }

    private static List<Map<String, String>> getSearchResults() {
        return Stream.of(PageStatus.values())
                .map(x -> ImmutableMap.of("id", x.name(), "text", x.humanName()))
                .collect(toList());
    }
}
